"""
AMM with lending
"""

import json
import math
from decimal import Decimal

from dys import (
    BLOCK_INFO,
    CALLER,
    SCRIPT_ADDRESS,
    _chain,
    get_coins_sent,
    get_nfts_sent,
)

DYS_NAME = "whaleswap.dys"


def _get_pool_index(pool_id: int) -> str:
    return f"{SCRIPT_ADDRESS}/pools/{int(pool_id):015}"


def _get_next_pool_id():
    index = f"{SCRIPT_ADDRESS}/next_pool_id"
    res = _chain("dyson/QueryStorage", index=index)
    if res["error"]:
        next_id = 1
    else:
        next_id = int(res["result"]["storage"]["data"])
    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=index,
        data=str(next_id + 1),
        force=True,
    )
    assert result["error"] is None, f"Error updating storage: {result['error']}"
    return next_id


def get_pool(pool_id: int):
    result = _chain("dyson/QueryStorage", index=_get_pool_index(pool_id))
    assert result["error"] is None, f"Error getting pool: {result['error']}"
    pool = json.loads(
        result["result"]["storage"]["data"],
        parse_float=Decimal,
        parse_int=Decimal,
    )
    return pool


def get_shares_denom(pool_id: int):
    return f"pool-{pool_id}.{DYS_NAME}"


def create_pool():
    coins = get_coins_sent()
    assert len(coins) == 2, "Both base and quote (DYS) must be sent to create the pool"

    # set base and quote coins, the quote is the denominted DYS
    if coins[0]["denom"] == "dys":
        base_coin = coins[1]
        quote_coin = coins[0]
    elif coins[1]["denom"] == "dys":
        base_coin = coins[0]
        quote_coin = coins[1]
    else:
        raise Exception("No DYS coins sent, cannot create pool.")

    pool_id = _get_next_pool_id()
    pool_index = _get_pool_index(pool_id)

    initial_shares = Decimal("100000")
    shares_denom = get_shares_denom(pool_id)
    pool = {
        "pool_id": pool_id,
        "base": {
            "denom": base_coin["denom"],
            "balance": Decimal(base_coin["amount"]),
            "lent": 0,
            "collateral": 0,
        },
        "quote": {
            "denom": quote_coin["denom"],
            "balance": Decimal(quote_coin["amount"]),
            "lent": 0,
            "collateral": 0,
        },
        "total_shares": initial_shares,
        "shares_denom": shares_denom,
        "block_height": BLOCK_INFO.height,
        "timestamp": BLOCK_INFO.time,
    }

    result = _chain(
        "dyson/sendMsgCreateStorage",
        creator=SCRIPT_ADDRESS,
        index=pool_index,
        data=json.dumps(pool),
    )
    assert result["error"] is None, f"Error creating storage: {result['error']}"

    result = _chain(
        "names/sendMsgMintCoins",
        owner=SCRIPT_ADDRESS,
        amount=f"{initial_shares} {shares_denom}",
    )
    assert result["error"] is None, f"Error minting coins: {result['error']}"

    result = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=CALLER,
        amount=[{"amount": str(initial_shares), "denom": shares_denom}],
    )
    assert result["error"] is None, f"Error sending coins: {result['error']}"

    return pool


def join_pool(pool_id: int):
    coins = get_coins_sent()
    assert len(coins) == 2, "Both base and quote (DYS) must be sent to join the pool"

    # set base and quote coins, the quote is the denominted DYS
    if coins[0]["denom"] == "dys":
        base_coin = coins[1]
        quote_coin = coins[0]
    elif coins[1]["denom"] == "dys":
        base_coin = coins[0]
        quote_coin = coins[1]
    else:
        raise Exception("No DYS coins sent, cannot join pool.")

    pool = get_pool(pool_id)

    sent_base_amount = Decimal(base_coin["amount"])
    sent_quote_amount = Decimal(quote_coin["amount"])

    pool_base = pool["base"]
    pool_quote = pool["quote"]

    correct_base_amount = (sent_quote_amount * Decimal(pool_base["balance"])) // (
        pool_quote["balance"]
    )
    correct_quote_amount = (sent_base_amount * (pool_quote["balance"])) // (
        pool_base["balance"]
    )
    refund = []
    if sent_base_amount > correct_base_amount:
        refund_amount = sent_base_amount - correct_base_amount
        refund_denom = base_coin["denom"]

        result = _chain(
            "cosmos.bank.v1beta1/sendMsgSend",
            from_address=SCRIPT_ADDRESS,
            to_address=CALLER,
            amount=[{"amount": str(refund_amount), "denom": refund_denom}],
        )
        assert result["error"] is None, f"Error sending refund: {result['error']}"
        refund += [{"amount": refund_amount, "denom": refund_denom}]

    if sent_quote_amount > correct_quote_amount:
        refund_amount = sent_quote_amount - correct_quote_amount
        refund_denom = quote_coin["denom"]

        result = _chain(
            "cosmos.bank.v1beta1/sendMsgSend",
            from_address=SCRIPT_ADDRESS,
            to_address=CALLER,
            amount=[{"amount": str(refund_amount), "denom": refund_denom}],
        )
        assert result["error"] is None, f"Error sending refund: {result['error']}"
        refund += [{"amount": refund_amount, "denom": refund_denom}]

    # update pool balances
    pool["base"]["balance"] = (pool_base["balance"]) + sent_base_amount
    pool["quote"]["balance"] = (pool_quote["balance"]) + sent_quote_amount

    # calculate shares
    base_shares = (sent_base_amount * pool["total_shares"]) // pool_base["balance"]
    quote_shares = (sent_quote_amount * pool["total_shares"]) // pool_quote["balance"]

    # in case there is a rounding difference, give the smaller share amount
    shares = min(base_shares, quote_shares)

    pool_index = _get_pool_index(pool_id)
    shares_denom = get_shares_denom(pool_id)

    result = _chain(
        "names/sendMsgMintCoins",
        owner=SCRIPT_ADDRESS,
        amount=f"{shares} {shares_denom}",
    )
    assert result["error"] is None, f"Error minting shares: {result['error']}"
    pool["total_shares"] = Decimal(pool["total_shares"]) + shares
    result = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=CALLER,
        amount=[{"amount": str(shares), "denom": shares_denom}],
    )
    assert result["error"] is None, f"Error sending shares: {result['error']}"

    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=pool_index,
        data=json.dumps(pool),
    )
    assert result["error"] is None, f"Error updating pool torage {result['error']}"

    # pool_id, shares, refunded amount and denom
    return {
        "pool_id": pool_id,
        "shares": shares,
        "share_denom": shares_denom,
        "refund": refund,
    }


def exit_pool(pool_id: int):
    coins = get_coins_sent()
    assert len(coins) == 1, "Only the shares denom must be sent to exit the pool"

    shares_denom = coins[0]["denom"]
    sent_shares_amount = Decimal(coins[0]["amount"])

    needed_denom = get_shares_denom(pool_id)
    assert (
        shares_denom == needed_denom
    ), f"Invalid shares denom, sent [{shares_denom}] needed [{needed_denom}] "

    pool = get_pool(pool_id)

    result = _chain("cosmos.bank.v1beta1/QuerySupplyOf", denom=shares_denom)
    assert result["error"] is None, f"Error getting total shares: {result['error']}"
    total_shares = result["result"]["amount"]
    total_shares_amount = Decimal(total_shares["amount"])

    # assert the total shores matches the pool total shares .
    # this shouldn't happen.
    assert (
        total_shares_amount == pool["total_shares"]
    ), f"Total shares mismatch, pool {pool['total_shares']} != total {total_shares_amount}"

    base_amount = (
        sent_shares_amount * Decimal(pool["base"]["balance"])
    ) // total_shares_amount
    quote_amount = (
        sent_shares_amount * Decimal(pool["quote"]["balance"])
    ) // total_shares_amount

    result = _chain(
        "names/sendMsgBurnCoins",
        owner=SCRIPT_ADDRESS,
        amount=f"{sent_shares_amount} {shares_denom}",
    )
    assert result["error"] is None, f"Error burning shares: {result['error']}"
    pool["total_shares"] = Decimal(pool["total_shares"]) - sent_shares_amount

    # update the base and denom on the pool
    pool["base"]["balance"] = Decimal(pool["base"]["balance"]) - base_amount
    pool["quote"]["balance"] = Decimal(pool["quote"]["balance"]) - quote_amount
    amount = []

    if base_amount:
        amount += [
            {"amount": str(base_amount), "denom": pool["base"]["denom"]},
        ]
    if quote_amount:
        amount += [
            {"amount": str(quote_amount), "denom": pool["quote"]["denom"]},
        ]

    amount = sorted(
        amount,
        key=lambda x: x["denom"],
    )
    result = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=CALLER,
        amount=amount,
    )
    assert result["error"] is None, f"Error sending coins: {result['error']}"

    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=_get_pool_index(pool_id),
        data=json.dumps(pool),
    )
    assert (
        result["error"] is None
    ), f"Error updating updating storage: {result['error']}"

    return amount


def swap(pool_ids: str, minimum_swap_out_amount: int, swap_out_denom: str):
    coins = get_coins_sent()
    assert len(coins) == 1, "One and only one coin denom must be sent for swapping"

    pool_ids = str(pool_ids)
    minimum_swap_out_amount = Decimal(minimum_swap_out_amount)
    input_amount = Decimal(coins[0]["amount"])
    input_denom = coins[0]["denom"]

    for pool_id in pool_ids.split():
        pool = get_pool(pool_id)

        K = pool["base"]["balance"] * pool["quote"]["balance"]

        if input_denom == pool["base"]["denom"]:
            pool["base"]["balance"] += input_amount
            output_amount = math.floor(
                pool["quote"]["balance"] - (K / pool["base"]["balance"])
            )
            assert output_amount, "Swap size too small"
            pool["quote"]["balance"] -= output_amount
            assert pool["quote"]["balance"] > 0, "Swap size too large"
            output_denom = pool["quote"]["denom"]
        elif input_denom == pool["quote"]["denom"]:
            pool["quote"]["balance"] += input_amount
            output_amount = math.floor(
                pool["base"]["balance"] - (K / pool["quote"]["balance"])
            )
            assert output_amount, "Swap size too small"
            pool["base"]["balance"] -= output_amount
            assert pool["base"]["balance"] > 0, "Swap size too large"
            output_denom = pool["base"]["denom"]
        else:
            raise Exception(
                f'input denom must be one of : [{pool["base"]["denom"]}, {pool["quote"]["denom"]}]'
            )
        result = _chain(
            "dyson/sendMsgUpdateStorage",
            creator=SCRIPT_ADDRESS,
            index=_get_pool_index(pool_id),
            data=json.dumps(pool),
        )
        assert (
            result["error"] is None
        ), f"Error updating updating storage: {result['error']}"

    if output_amount < minimum_swap_out_amount:
        raise Exception(
            f"Minimum output amount not reached: {output_amount} {output_denom} < {minimum_swap_out} {output_denom}"
        )
    if swap_out_denom != output_denom:
        raise Exception(
            f"Output denom doesn't match, wanted: {swap_out_denom} got: {output_denom}"
        )

    result = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=CALLER,
        amount=[{"amount": str(output_amount), "denom": output_denom}],
    )
    assert result["error"] is None, f"Error sending coins: {result['error']}"

    return {"output_amount": output_amount, "output_denom": output_denom}


# Helper function to get the loan index
def get_loan_index(pool_id: int, loan_id: int):
    return f"{SCRIPT_ADDRESS}/pools/{int(pool_id):015}/loans/{int(loan_id):015}"


def get_loan_nft_id(pool_id: int, loan_id: int):
    return f"{pool_id}-{loan_id}"


def get_loan_nft_class_id():
    return f"loans.{DYS_NAME}"


# Helper function to get the next loan ID
def _get_next_loan_id(pool_id: int):
    index = f"{SCRIPT_ADDRESS}/pools/{int(pool_id):015}/next_loan_id"
    res = _chain("dyson/QueryStorage", index=index)
    if res["error"]:
        next_id = 1
    else:
        next_id = int(res["result"]["storage"]["data"])
    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=index,
        data=str(next_id + 1),
        force=True,
    )
    assert result["error"] is None, f"Error updating storage: {result['error']}"
    return next_id


# Borrow function
def borrow(pool_id: int, borrowed_amount: int, borrowed_denom: str):
    from decimal import Decimal

    coins = get_coins_sent()
    if len(coins) != 1:
        raise ValueError("Exactly one collateral denom must be sent to borrow")

    collateral_denom = coins[0]["denom"]
    collateral_amount = Decimal(coins[0]["amount"])

    pool = get_pool(pool_id)

    if (
        collateral_denom == pool["base"]["denom"]
        and borrowed_denom == pool["quote"]["denom"]
    ):
        collateral_pool = pool["base"]
        borrow_pool = pool["quote"]
    elif (
        collateral_denom == pool["quote"]["denom"]
        and borrowed_denom == pool["base"]["denom"]
    ):
        collateral_pool = pool["quote"]
        borrow_pool = pool["base"]
    else:
        raise ValueError(
            "Collateral denom must be either base or quote in the pool,"
            " and borrowed denom must be the opposite"
        )

    total_max_borrowable = borrow_pool["balance"] // 3
    available_borrowable = total_max_borrowable - borrow_pool["lent"]

    collateral_price = collateral_pool["balance"] / borrow_pool["balance"]
    min_collateral_value = collateral_price * borrowed_amount * 3

    if collateral_amount < min_collateral_value:
        raise ValueError(
            f"set collateral value [{collateral_amount} {collateral_denom}] must be at least 3x of the desired borrowed value [{min_collateral_value} {collateral_denom}]"
        )

    borrow_pool["lent"] += borrowed_amount
    collateral_pool["collateral"] += collateral_amount

    if borrowed_amount > available_borrowable:
        raise ValueError(
            "Borrowed amount exceeds the pool's lending limit:"
            f" {available_borrowable} {borrowed_denom}"
        )

    update_pool_result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=_get_pool_index(pool_id),
        data=json.dumps(pool),
    )
    if update_pool_result["error"] is not None:
        raise Exception(f"Error updating storage: {update_pool_result['error']}")

    loan_id = _get_next_loan_id(pool_id)
    loan = {
        "loan_id": loan_id,
        "pool_id": pool_id,
        "collateral_amount": collateral_amount,
        "collateral_denom": collateral_denom,
        "borrowed_amount": borrowed_amount,
        "borrowed_denom": borrowed_denom,
        "interest_fee": 0,
    }

    result = _chain(
        "names/sendMsgMintNft",
        class_owner=SCRIPT_ADDRESS,
        class_id=get_loan_nft_class_id(),
        id=get_loan_nft_id(pool_id, loan_id),
        uri="",
        uri_hash="",
    )
    assert result["error"] is None, f"Error minting loan NFT: {result['error']}"

    _chain(
        "cosmos.nft.v1beta1/sendMsgSend",
        class_id=get_loan_nft_class_id(),
        id=get_loan_nft_id(pool_id, loan_id),
        sender=SCRIPT_ADDRESS,
        receiver=CALLER,
    )
    assert result["error"] is None, f"Error sending loan NFT: {result['error']}"

    create_storage_result = _chain(
        "dyson/sendMsgCreateStorage",
        creator=SCRIPT_ADDRESS,
        index=get_loan_index(pool_id, loan_id),
        data=json.dumps(loan),
    )
    if create_storage_result["error"] is not None:
        raise Exception(f"Error creating storage: {create_storage_result['error']}")

    send_result = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=CALLER,
        amount=[{"amount": str(borrowed_amount), "denom": borrowed_denom}],
    )
    if send_result["error"] is not None:
        raise Exception(f"Error sending borrowed coins: {send_result['error']}")


# Repay function
def repay(pool_id: int, loan_id: int):
    coins = get_coins_sent()

    if len(coins) != 1:
        raise ValueError("Exactly one borrowed denom must be sent to repay")

    loan_nfts = get_nfts_sent()

    borrowed_denom = coins[0]["denom"]
    borrowed_amount = Decimal(coins[0]["amount"])

    pool = get_pool(pool_id)

    loan_index = get_loan_index(pool_id, loan_id)
    loan_result = _chain("dyson/QueryStorage", index=loan_index)
    assert loan_result["error"] is None, f"Error getting loan: {loan_result['error']}"
    loan = json.loads(
        loan_result["result"]["storage"]["data"],
        parse_float=Decimal,
        parse_int=Decimal,
    )

    if borrowed_denom != loan["borrowed_denom"]:
        raise ValueError("Repayment must be made in the borrowed denom")
    if borrowed_amount != loan["borrowed_amount"]:
        raise ValueError("The exact amount of coins must be sent to repay")

    nft_class_id = get_loan_nft_class_id()
    nft_id = get_loan_nft_id(pool_id, loan_id)

    if (
        len(loan_nfts) == 1
        and loan_nfts[0]["class_id"] == nft_class_id
        and loan_nft[0]["id"] == nft_id
    ):
        burn_result = _chain(
            "names/sendMsgBurnNft",
            class_owner=SCRIPT_ADDRESS,
            class_id=nft_class_id,
            id=nft_id,
        )
        if burn_result["error"] is not None:
            raise Exception(f"Error burning loan NFT: {update_pool_result['error']}")

    elif len(loan_nfts) == 0:
        # Check if the loan is undercollateralized and anyone can repay
        if loan["collateral_denom"] == pool["base"]["denom"]:
            collateral_pool = pool["base"]
            borrow_pool = pool["quote"]
        elif loan["collateral_denom"] == pool["quote"]["denom"]:
            collateral_pool = pool["quote"]
            borrow_pool = pool["base"]
        else:
            raise ValueError("Invalid collateral denom in the pool")
        collateral_price = collateral_pool["balance"] / borrow_pool["balance"]
        current_value_of_borrowed = borrowed_amount * collateral_price
        collateral_value = loan["collateral_amount"]
        if collateral_value >= 2 * current_value_of_borrowed:
            raise ValueError(
                f"Cannot be liquidated, must attach loan NFT to repay this loan: {nft_class_id}/{nft_id}"
            )
    else:
        raise ValueError(
            f"Must attach correct loan NFT to repay: {nft_class_id}/{nft_id}"
        )

    # Repay the loan
    borrow_pool = (
        pool["quote"] if pool["quote"]["denom"] == borrowed_denom else pool["base"]
    )
    borrow_pool["lent"] -= borrowed_amount
    update_pool_result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=_get_pool_index(pool_id),
        data=json.dumps(pool),
    )
    if update_pool_result["error"] is not None:
        raise Exception(f"Error updating storage: {update_pool_result['error']}")
    # Send collateral back to the borrower
    send_result = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=loan["borrower"],
        amount=[
            {
                "amount": str(loan["collateral_amount"]),
                "denom": loan["collateral_denom"],
            }
        ],
    )
    if send_result["error"] is not None:
        raise Exception(f"Error sending collateral coins: {send_result['error']}")
    # Delete the loan
    delete_loan_result = _chain(
        "dyson/sendMsgDeleteStorage",
        creator=SCRIPT_ADDRESS,
        index=loan_index,
    )
    if delete_loan_result["error"] is not None:
        raise Exception(f"Error deleting storage: {delete_loan_result['error']}")

