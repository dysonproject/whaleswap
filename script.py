import json
from dys import _chain, SCRIPT_ADDRESS, CALLER, get_coins_sent, BLOCK_INFO
import typing
from decimal import Decimal


"""
Basic Auction
"""


def _get_next_auction_id():
    index = f"{SCRIPT_ADDRESS}/next_auction_id"
    res = _chain("dyson/QueryStorage", index=index)
    if res["error"]:
        next_id = 1
    else:
        next_id = int(res["result"]["storage"]["data"])
    _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=index,
        data=str(next_id + 1),
        force=True,
    )
    return next_id


def get_auction_index(auction_id: int):
    return f"{SCRIPT_ADDRESS}/auctions/{auction_id:015}"


def get_auction(auction_id: int):
    """
    Get the auction data for the given auction_id

    Args:
    auction_id (int): The auction_id to get the data for
    """

    return json.loads(
        _chain("dyson/QueryStorage", index=get_auction_index(auction_id))["result"][
            "storage"
        ]["data"]
    )


def create_auction(
    description: typing.Annotated[str, '{"format":"textarea"}'],
    bid_denom: str,
    duration: int,
    start_block_height: int = None,
):
    """
    Create a new auction, the caller will be the owner of the auction

    Args:
    description (str): A description of the auction
    bid_denom (str): The coin denom to use for bidding
    duration (int): The duration of the auction in terms of block height
    start_block_height (int, optional): The block height the auction will start at, defaults to current block height
    """
    coins = get_coins_sent()

    if start_block_height is None:
        start_block_height = BLOCK_INFO.height

    end_block_height = start_block_height + duration

    assert (
        start_block_height < end_block_height
    ), "start_block_height must be less than end_block_height"
    assert (
        start_block_height >= BLOCK_INFO.height
    ), "start_block_height must be in the future"

    auction_id = _get_next_auction_id()
    auction_index = get_auction_index(auction_id)

    auction = {
        "auction_id": auction_id,
        "owner": CALLER,
        "description": description,
        "coins": coins,
        "bid_denom": bid_denom,
        "start_block_height": start_block_height,
        "end_block_height": end_block_height,
        "highest_bid": 0,
        "highest_bidder": "",
        "bids": [],
    }
    _chain(
        "dyson/sendMsgCreateStorage",
        creator=SCRIPT_ADDRESS,
        index=auction_index,
        data=json.dumps(auction),
    )
    print(f"created auction_id={auction_id}")

    return auction


def claim_auction(auction_id: int):
    """
    End an auction, the highest bidder will get the coins or the owner will get the coins back if no bids were made.

    Args:
    auction_id (int): The id of the auction to end
    """

    auction = get_auction(auction_id)

    assert auction["end_block_height"] <= BLOCK_INFO.height, "auction has not ended yet"

    if Decimal(auction["highest_bid"]) > 0:
        # only the bidder can end the auction
        assert (
            CALLER == auction["highest_bidder"]
        ), "only the highest bidder can end this auction"

        # send coins to highest bidder
        ret = _chain(
            "cosmos.bank.v1beta1/sendMsgSend",
            from_address=SCRIPT_ADDRESS,
            to_address=auction["highest_bidder"],
            amount=auction["coins"],
        )
    else:
        # only the owner can end the auction
        assert CALLER == auction["owner"], "only the owner can end this auction"

        # refund coins to owner
        ret = _chain(
            "cosmos.bank.v1beta1/sendMsgSend",
            from_address=SCRIPT_ADDRESS,
            to_address=auction["owner"],
            amount=auction["coins"],
        )

    assert ret["error"] is None, f"Error sending: {ret}"

    _chain(
        "dyson/sendMsgDeleteStorage",
        creator=SCRIPT_ADDRESS,
        index=get_auction_index(auction_id),
    )

    print(f"ended auction_id={auction_id}")
    return auction


def bid(auction_id: int):
    """
    Bid on an auction, the bid must be higher than the current highest bid.
    Bid by sending coins to this function call, the coins will be set for the auction.
    The previously highest bidder will be refunded their coins.

    Args:
    auction_id (int): The id of the auction to bid on
    """

    coins = get_coins_sent()
    assert len(coins) == 1, "only one coin denom can be sent"

    auction = get_auction(auction_id)

    assert (
        auction["start_block_height"] <= BLOCK_INFO.height
    ), f"auction has not started yet, {auction['start_block_height'] - BLOCK_INFO.height} blocks"

    assert (
        auction["end_block_height"] > BLOCK_INFO.height
    ), f"auction has ended: {auction['end_block_height'] - BLOCK_INFO} blocks"

    assert (
        coins[0]["denom"] == auction["bid_denom"]
    ), f"sent coin denom[{coins[0]['denom']}] != auction coin denom[{auction['bid_denom']}]"

    assert Decimal(coins[0]["amount"]) > Decimal(
        auction["highest_bid"]
    ), f"sent coin amount[{coins[0]['amount']}] <= highest bid[{auction['highest_bid']}]"

    if auction["highest_bid"] > 0:
        ret = _chain(
            "cosmos.bank.v1beta1/sendMsgSend",
            from_address=SCRIPT_ADDRESS,
            to_address=auction["highest_bidder"],
            amount=[{"amount": auction["highest_bid"], "denom": auction["bid_denom"]}],
        )
        assert ret["error"] is None, f"Error sending: {ret}"

    auction["highest_bid"] = coins[0]["amount"]
    auction["highest_bidder"] = CALLER

    auction["bids"].append(
        {
            "bidder": CALLER,
            "amount": coins[0]["amount"],
            "block_height": BLOCK_INFO.height,
        }
    )

    _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=get_auction_index(auction_id),
        data=json.dumps(auction),
    )

    print(f"bid on auction_id={auction_id}")
    return auction


"""
Baisc Orderbook
"""


def _get_next_offer_id():
    index = f"{SCRIPT_ADDRESS}/next_offer_id"
    res = _chain("dyson/QueryStorage", index=index)
    if res["error"]:
        next_id = 1
    else:
        next_id = int(res["result"]["storage"]["data"])
    _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=index,
        data=str(next_id + 1),
        force=True,
    )
    return next_id


def get_offer_index(offer_id: int):
    return f"{SCRIPT_ADDRESS}/offers/{offer_id:015}"


def get_offer(offer_id: int):
    return json.loads(
        _chain("dyson/QueryStorage", index=get_offer_index(offer_id))["result"][
            "storage"
        ]["data"]
    )


def offer_coins(requested_amount: str, requested_denom: str):
    coins = get_coins_sent()
    assert len(coins) == 1, "only one coin denom can be sent"

    offer_id = _get_next_offer_id()
    offer_index = get_offer_index(offer_id)

    offer = {
        "offer_id": offer_id,
        "address": CALLER,
        "offered_amount": coins[0]["amount"],
        "offered_denom": coins[0]["denom"],
        "requested_amount": requested_amount,
        "requested_denom": requested_denom,
    }
    _chain(
        "dyson/sendMsgCreateStorage",
        creator=SCRIPT_ADDRESS,
        index=offer_index,
        data=json.dumps(offer),
    )

    print(f"created offer_id={offer_id}")
    return offer


def accept_offer(offer_id: int):
    coins = get_coins_sent()
    assert len(coins) == 1, "only one coin denom can be sent"

    sent_amount = coins[0]["amount"]
    sent_denom = coins[0]["denom"]

    offer = get_offer(offer_id)

    assert (
        sent_amount == offer["requested_amount"]
    ), f"sent_amount[{sent_amount}] != requested_amount[{offer['requested_amount']}]"
    assert (
        sent_denom == offer["requested_denom"]
    ), f"sent_denom[{sent_denom}] != requested_denom[{offer['requested_denom']}]"

    ret = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=offer["address"],
        amount=coins,
    )
    assert ret["error"] is None, f"Error sending: {ret}"

    ret = _chain(
        "cosmos.bank.v1beta1/sendMsgSend",
        from_address=SCRIPT_ADDRESS,
        to_address=CALLER,
        amount=[{"amount": offer["offered_amount"], "denom": offer["offered_denom"]}],
    )
    assert ret["error"] is None, f"Error sending: {ret}"

    _chain(
        "dyson/sendMsgDeleteStorage",
        creator=SCRIPT_ADDRESS,
        index=get_offer_index(offer_id),
    )
    print(f"accepted offer_id={offer_id}")
    return offer

# Dwapp


def application(environ, start_response):
    start_response("200 ok", [("Content-Type", "text/html")])
    return [
        """<html data-theme="dark">

  <head>
    <title>WhaleSwap</title>

    <link href="https://cdn.jsdelivr.net/npm/daisyui@2.51.5/dist/full.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2/dist/tailwind.min.css" rel="stylesheet" type="text/css" />


    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    <div id="app">{{ message }}</div>

    <script>
      const {
        createApp
      } = Vue

      createApp({
          data() {
            return {
              message: 'Hello Vue!'
            }
          }
        })
        .mount('#app')
    </script>
  </head>

  <body></body>

</html>""".encode()
    ]

