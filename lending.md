Automated Market Maker (AMM) - Technical Specification
======================================================

This document provides the technical specification for updating the existing Automated Market Maker (AMM) in Python to include the ability to borrow and repay coins.

Overview
--------

The current AMM implementation provides functionality for creating pools, joining and exiting pools, and performing swaps between different coins. This document proposes the addition of two new features: borrow and repay.

-   Borrow: Allows users to deposit coins as collateral to the script and receive the equivalent amount in another denom from the pool.
-   Repay: Allows users to send the borrowed amount back to the pool and receive their original collateral.

Requirements
------------

1.  Update the existing AMM implementation to include the ability to borrow and repay coins.
2.  Maintain the functionality of the existing functions.
3.  Store loans in their own storage with a reference to the loan_id and pool_id in the index.

Specification
-------------

### Functions

### Functions

#### `borrow(pool_id: int)`

This function allows users to deposit coins to the script as collateral and borrow the equivalent amount in the other denom in the pool. The pool is limited to lending a maximum of 1/3 of the denom in the pool, and the global minimum collateralization ratio is 3x. The collateral and borrowed asset price feed are obtained from the pool that is lending, using the ratio of coin_a and coin_b. The interest fee is dynamically set to the ratio of the total amount borrowed to the total amount remaining of that denom in the pool at the time of the loan.

Input:

-   `pool_id`: The ID of the pool from which the user wants to borrow.

Output:

-   `loan`: A dictionary containing the loan details (e.g., loan_id, borrower, collateral_amount, collateral_denom, borrowed_amount, borrowed_denom, interest_fee).

Steps:

1.  Get the collateral sent by the user.
2.  Calculate the maximum amount that can be borrowed from the pool (1/3 of the denom in the pool).
3.  Calculate the equivalent amount to be borrowed in the other denom, using the coin_a and coin_b ratio from the lending pool.
4.  Verify that the calculated amount is within the pool's lending limit and maintains a minimum 3x collateralization ratio.
5.  Calculate the interest fee as the ratio of the total amount borrowed to the total amount remaining of that denom in the pool at the time of the loan.
6.  Update the pool balances.
7.  Create a new loan object with the loan details, including the interest fee.
8.  Store the loan in its own storage with a reference to the loan_id and pool_id in the index.
9.  Send the borrowed amount to the user.

#### `repay(pool_id: int, loan_id: int)`

This function allows users to repay the borrowed amount. The fee is 1% of the loan value denominated in pool shares. If the user does not send the correct fee, the function will use the collateral to buy the required pool shares and return the remaining collateral to the user. If the collateralization ratio of the loan drops below 2x, any user can repay the loan and receive the collateral, effectively liquidating the loan. The collateral and borrowed asset price feed are obtained from the pool that is lending, using the ratio of coin_a and coin_b.

Input:

-   `pool_id`: The ID of the pool where the loan was taken.
-   `loan_id`: The ID of the loan being repaid.

Output:

-   `loan`: A dictionary containing the updated loan details (e.g., loan_id, borrower, collateral_amount, collateral_denom, repaid_amount, repaid_denom).

Steps:

1.  Get the borrowed amount and any pool shares sent by the user.
2.  Verify that the user has sent the correct borrowed amount.
3.  Calculate the current collateralization ratio of the loan, using the coin_a and coin_b ratio from the lending pool.
4.  If the collateralization ratio is below 2x:
    1.  Allow any user to repay the loan.
    2.  Send the collateral to the user repaying the loan.
5.  Update the pool balances.
6.  Calculate the fee as 1% of the loan value denominated in pool shares.
7.  If the user has not sent the correct fee:
    1.  Use part of the collateral to buy the required pool shares.
    2.  Burn the pool shares equivalent to the fee.
    3.  Update the remaining collateral amount.
8.  Update the loan object to reflect that it has been repaid.
9.  Send the remaining collateral back to the user.

Storage
-------

### Loan Storage

The loan storage will be used to store loan objects. Each loan object will have the following structure:

pythonCopy code

`{
    "loan_id": int,
    "pool_id": int,
    "borrower": str,
    "collateral_amount": Decimal,
    "collateral_denom": str,
    "borrowed_amount": Decimal,
    "borrowed_denom": str,
    "repaid": bool
}`

The storage index will be a combination of the loan_id and pool_id:

pythonCopy code

`f"{SCRIPT_ADDRESS}/loans/{int(loan_id):015}-{int(pool_id):015}"`

### Functions for Loan Storage

#### `get_loan_index(loan_id: int, pool_id: int) -> str`

Returns the storage index for the loan object.

#### `get_loan(loan_id: int, pool_id: int) -> dict`

Returns the loan object from the loan storage.

#### `_get_next_loan_id() -> int`

Returns the next available loan_id and updates the storage.
